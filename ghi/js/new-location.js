

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();
            const state_options = document.querySelector('#state');
            for(let state of data.states){
                let html = `<option value="${state.abbreviation}">${state.name}</option>`;
                state_options.innerHTML += html;
            }
        }
    } catch (e) {
        // Figure out what to do if an error is raised
    }
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async () => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
    });
});
