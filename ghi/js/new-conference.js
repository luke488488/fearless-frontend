window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();
            const location_options = document.querySelector('#location');
            for(let location of data.locations){
                let html = `<option value="${location.id}">${location.name}</option>`;
                location_options.innerHTML += html;
            }
        }
    } catch (e) {
        // Figure out what to do if an error is raised
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async () => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
        }
    });
});
